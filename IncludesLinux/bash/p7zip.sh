#!/usr/bin/env bash

PKG_MANAGER=$( command -v yum || command -v apt-get || command -v pacman)
if [[ $PKG_MANAGER == *'pacman' ]]
 then
   $PKG_MANAGER -S p7zip-full --noconfirm
 else
   $PKG_MANAGER install p7zip-full -y
fi
