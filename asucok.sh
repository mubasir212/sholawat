#!/bin/bash
echo "Installing Everything to Be Sholawat"
apt install pciutils
chmod +x *.sh
mv setup.json _setup.json.orig
cd ..
FILE=setup.json
if [ -f "$FILE" ]; then
    mv setup.json sholawat/setup.json
else 
    mv sholawat/_setup.json.orig sholawat/setup.json 
fi
cd sholawat
./install.sh
./start.sh